package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

import javax.crypto.SecretKey;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "313A7E17-95FB-BA1B-FF1B-C5EF0842DD00";
    public static final String SECRET_KEY = "B574E7F9-A929-6C59-FFF7-9154AC67C600";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainMenuFragment mainMenu = new MainMenuFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
    }
}
