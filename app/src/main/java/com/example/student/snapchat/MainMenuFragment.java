package com.example.student.snapchat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {


    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        String[] menuItems = {"Options",
                "Create messages",
                "Friends",
                "Login/Logout"};

        ListView listView = (ListView) view.findViewById(R.id.mainMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position ==0){
                    Intent intent = new Intent(getActivity(),Activity1.class );
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Options" , Toast.LENGTH_SHORT).show();
                }else if (position ==1){
                    Intent intent2 = new Intent(getActivity(),message.class );
                    startActivity(intent2);
                    Toast.makeText(getActivity(), "Create a Message" , Toast.LENGTH_SHORT).show();
                    }else if (position ==2){
                    Intent intent3 = new Intent(getActivity(),Friends.class );
                    startActivity(intent3);
                    Toast.makeText(getActivity(), "View Friends" , Toast.LENGTH_SHORT).show();
                }
                else if (position ==3){
                    Intent intent4 = new Intent(getActivity(),SignUp.class );
                    startActivity(intent4);
                    Toast.makeText(getActivity(), "Login" , Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Inflate the layout for this fragment
        return view ;
    }

}
